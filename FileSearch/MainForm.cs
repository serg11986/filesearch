﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace FileSearch
{ 
    public partial class MainForm : Form
    {
        #region Fields

        private Searcher searcher = new Searcher();
        private TimeSpan elapsedTime = TimeSpan.Zero;

        #endregion

        #region Constructor and FormLoad

        public MainForm()
        {
            InitializeComponent();

            searcher.SearchStarted += SearchStartedHandler;
            searcher.SearchFinished += SearchFinishedHandler;
            searcher.StoppingProcessStarted += StoppingProcessStartedHandler;
            searcher.StoppingProcessFinished += StoppingProcessFinishedHandler;

            timer.Interval = 10;
            timer.Tick += (_, __) =>
            {
                elapsedTime = elapsedTime.Add(TimeSpan.FromMilliseconds(timer.Interval));
                lblTimeElapsed.Text = elapsedTime + "";
                DisplayCurSearcherState();
            };
        }               

        private void MainForm_Load(object sender, EventArgs e)
        {
            DisplayControllerSearchSettings();
            ArrangeControlsForStoppedState();
        }

        #endregion

        #region Button clicks handlers

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(tbStartFolder.Text.Trim()))
            {
                MessageBox.Show("Проверьте корректность пути стартовой директории");
                return;
            }
            searcher.SearchSettings.SearchString = tbSearchString.Text.Trim();
            searcher.SearchSettings.StartFolder
                = tbStartFolder.Text.Trim();
            searcher.SearchSettings.FileNameTemplate = tbFileNameTemplate.Text.Trim();
            if (searcher.WorkingState == WorkingState.Stopped)
            {
                searcher.Start();
            }
            else if (searcher.WorkingState == WorkingState.Paused)
            {
                searcher.Resume();
                timer.Start();
                ArrangeControlsForWorkingState();
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            searcher.Pause();
            timer.Stop();
            ArrangeControlsForPausedState();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            searcher.RestartAsync();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            searcher.StopAsync();
            ArrangeControlsForStoppedState();
        }

        private void btnChooseStartFolder_Click(object sender, EventArgs e)
        {
            using (var folderBrowserDialor = new FolderBrowserDialog())
            {
                if (folderBrowserDialor.ShowDialog() == DialogResult.OK)
                {
                    tbStartFolder.Text = searcher.SearchSettings.StartFolder =
                        folderBrowserDialor.SelectedPath;
                }
            }
        }

        #endregion

        #region Arrange controls and display info methods

        private void ArrangeControlsForPausedState()
        {
            btnPause.Enabled = false;
            btnStart.Enabled = btnStop.Enabled = btnRestart.Enabled = true;
            tbFileNameTemplate.ReadOnly = tbSearchString.ReadOnly
                = tbStartFolder.ReadOnly = true;
            btnChooseStartFolder.Enabled = false;
            headerSearchString.Enabled = headerStartFolder.Enabled
                = headerFileNameTemplate.Enabled = false;
        }

        private void ArrangeControlsForStoppedState()
        {
            btnStop.Enabled = btnPause.Enabled = btnRestart.Enabled = false;
            btnStart.Enabled = true;
            tbFileNameTemplate.ReadOnly = tbSearchString.ReadOnly
                = tbStartFolder.ReadOnly = false;
            btnChooseStartFolder.Enabled = true;
            headerSearchString.Enabled = headerStartFolder.Enabled
                = headerFileNameTemplate.Enabled = true;

        }

        private void ArrangeControlsForWorkingState()
        {
            btnStart.Enabled = false;
            btnStop.Enabled = btnPause.Enabled = btnRestart.Enabled = true;
            tbFileNameTemplate.ReadOnly = tbSearchString.ReadOnly
                = tbStartFolder.ReadOnly = true;
            btnChooseStartFolder.Enabled = false;
            headerSearchString.Enabled = headerStartFolder.Enabled
                = headerFileNameTemplate.Enabled = false;
        }

        private void ClearControlsAndStatsForStartOrRestart()
        {
            lblCurProcessingFile.Text = lblCurProcessingFolder.Text = "";
            elapsedTime = TimeSpan.Zero;
            lblTimeElapsed.Text = TimeSpan.Zero + "";
            lblProcessedFilesCount.Text = "0";
            treeViewFoundFiles.Nodes.Clear();
        }

        private void DisplayControllerSearchSettings()
        {
            tbSearchString.Text = searcher.SearchSettings.SearchString;
            tbStartFolder.Text = searcher.SearchSettings.StartFolder;
            tbFileNameTemplate.Text = searcher.SearchSettings.FileNameTemplate;
        }

        private void DisplayCurSearcherState()
        {
            lblCurProcessingFile.Text = searcher.CurProcessingFile;
            lblProcessedFilesCount.Text = searcher.ProcessedFilesCount + "";
            lblCurProcessingFolder.Text = searcher.CurProcessingFolder;
            if (searcher.MatchingFiles.Count != 0)
                AddNode(searcher.MatchingFiles.Dequeue());
        }


        #endregion

        #region Manipulations with TreeNode

        private void treeViewFoundFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (treeViewFoundFiles.SelectedNode != null
                && treeViewFoundFiles.SelectedNode.Nodes.Count == 0)
            {
                new FormViewFileText
                  (GetFullPath(treeViewFoundFiles.SelectedNode)).Show();
            }
        }

        private string GetFullPath(TreeNode node)
        {
            var stringBuilder = new StringBuilder(node.Name);
            TreeNode parent = node.Parent;
            while (parent != null)
            {
                stringBuilder.Insert(0, parent.Name + "\\");
                parent = parent.Parent;
            }
            return stringBuilder.ToString();
        }

        private void AddNode(string fullFilePath)
        {
            string initFolder = searcher.SearchSettings.StartFolder.Trim(' ', '\\');
            if (fullFilePath.Substring(0, initFolder.Length) != initFolder)
            {
                MessageBox.Show("Файл " + fullFilePath
                    + " не принадлежит заданной начальной директории");
                return;
            }
            if (!treeViewFoundFiles.Nodes.ContainsKey(initFolder))
                treeViewFoundFiles.Nodes.Add(initFolder, initFolder);

            string relativePath = fullFilePath.Substring(initFolder.Length + 1);
            string[] relativePathComponents = relativePath.Split('\\');
            TreeNode parentNode = treeViewFoundFiles.Nodes[initFolder];
            foreach (string curNodeName in relativePathComponents)
            {
                if (!parentNode.Nodes.ContainsKey(curNodeName))
                    parentNode = parentNode.Nodes.Add(curNodeName, curNodeName);
                else
                    parentNode = parentNode.Nodes[curNodeName];
            }
        }

        #endregion

        #region Searchers's events handlers


        private void SearchStartedHandler()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(SearchStartedHandler));
                return;
            }

            ClearControlsAndStatsForStartOrRestart();
            ArrangeControlsForWorkingState();
            timer.Start();
        }

        private void SearchFinishedHandler()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(SearchFinishedHandler));
                return;
            }

            while (searcher.MatchingFiles.Count != 0)
                AddNode(searcher.MatchingFiles.Dequeue());
            timer.Stop();
            ArrangeControlsForStoppedState();
        }



        private void StoppingProcessStartedHandler()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(StoppingProcessStartedHandler));
                return;
            }

            Cursor = Cursors.WaitCursor;
        }


        private void StoppingProcessFinishedHandler()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(StoppingProcessFinishedHandler));
                return;
            }


            while (searcher.MatchingFiles.Count != 0)
                AddNode(searcher.MatchingFiles.Dequeue());
            Cursor = Cursors.Default;
            timer.Stop();
            ArrangeControlsForStoppedState();
        }

        
        #endregion

    }
}
