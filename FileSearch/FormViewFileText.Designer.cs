﻿namespace FileSearch
{
    partial class FormViewFileText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFileText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbFileText
            // 
            this.tbFileText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFileText.Location = new System.Drawing.Point(0, 0);
            this.tbFileText.Multiline = true;
            this.tbFileText.Name = "tbFileText";
            this.tbFileText.ReadOnly = true;
            this.tbFileText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbFileText.Size = new System.Drawing.Size(724, 504);
            this.tbFileText.TabIndex = 0;
            // 
            // FormViewFileText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 504);
            this.Controls.Add(this.tbFileText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormViewFileText";
            this.ShowIcon = false;
            this.Text = "FormViewFileText";
            this.Load += new System.EventHandler(this.FormViewFileText_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFileText;
    }
}