﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;

namespace FileSearch
{
    public enum WorkingState { Stopped, Paused, Working }

    public class Searcher
    {
        #region Fields

        ManualResetEvent signal = new ManualResetEvent(initialState: true);
        bool shouldStop;
        Thread workingThread;
        int curThreadNum;
        const string settingsFileName = "settings.json";

        #endregion

        #region Props

        public SearchSettings SearchSettings { get; }
        public WorkingState WorkingState { get; private set; } = WorkingState.Stopped;
        public int ProcessedFilesCount { get; private set; }
        public string CurProcessingFolder { get; private set; }
        public string CurProcessingFile { get; private set; }
        public Queue<string> MatchingFiles { get; private set; } = new Queue<string>();

        #endregion

        #region Events

        public event Action SearchStarted;
        public event Action SearchFinished;
        public event Action StoppingProcessStarted;
        public event Action StoppingProcessFinished;

        #endregion

        #region Constructor and finalizer

        public Searcher()
        {
            SearchSettings = LoadSettings();
        }

        ~Searcher() => SaveSettings();

        #endregion

        #region Load and save SearchSettings

        private SearchSettings LoadSettings()
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(SearchSettings));
            try
            {
                using (FileStream fs = new FileStream(settingsFileName, FileMode.OpenOrCreate))
                {
                    return (SearchSettings)jsonFormatter.ReadObject(fs);
                }
            }
            catch
            {
                return new SearchSettings();
            }
        }

        private void SaveSettings()
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(SearchSettings));
            try
            {
                using (FileStream fs = new FileStream(settingsFileName, FileMode.Create))
                {
                    jsonFormatter.WriteObject(fs, SearchSettings);
                }
            }
            catch { }
        }

        #endregion

        #region Searching methods

        public void Search()
        {
            lock (signal)
            {
                signal.Set();
                shouldStop = false;
            }

            ProcessedFilesCount = 0;

            SearchStarted?.Invoke();

            string fileNameTemplate =
                string.IsNullOrWhiteSpace(SearchSettings.FileNameTemplate)
                ? "*" : SearchSettings.FileNameTemplate;

            var folders = new Queue<string>();
            folders.Enqueue(SearchSettings.StartFolder);

            while (folders.Count != 0)
            {
                string currentFolder = folders.Dequeue();
                CurProcessingFolder = currentFolder;

                signal.WaitOne();
                if (shouldStop)
                    return;

                try
                {
                    string[] filesInCurrentFolder
                        = Directory.GetFiles(currentFolder, fileNameTemplate);

                    foreach (string filePath in filesInCurrentFolder)
                    {
                        signal.WaitOne();
                        if (shouldStop)
                            return;

                        if (FileMatches(filePath, SearchSettings.SearchString))
                            MatchingFiles.Enqueue(filePath);

                        signal.WaitOne();
                        if (shouldStop)
                            return;
                    }
                }
                catch { }

                try
                {
                    string[] foldersInCurrentFolder
                        = Directory.GetDirectories(currentFolder);

                    foreach (string folderPath in foldersInCurrentFolder)
                        folders.Enqueue(folderPath);
                }
                catch { }

                signal.WaitOne();
                if (shouldStop)
                    return;
            }
            WorkingState = WorkingState.Stopped;
            SearchFinished?.Invoke();
        }

        private bool FileMatches(string filePath, string searchString)
        {
            CurProcessingFile = Path.GetFileName(filePath);

            signal.WaitOne();
            if (shouldStop)
                return false;

            try
            {
                foreach (string line in File.ReadLines(filePath, Encoding.Default))
                {
                    signal.WaitOne();
                    if (shouldStop)
                        return false;

                    if (line.Contains(searchString))
                        return true;

                    signal.WaitOne();
                    if (shouldStop)
                        return false;
                }
                return false;
            }
            catch
            {
                return false;
            }
            finally
            {
                ProcessedFilesCount++;
            }
        }


        #endregion

        #region Working process manipulation public methods

        public void Start()
        {
            workingThread = new Thread(Search);
            workingThread.Name = "thread " + ++curThreadNum + "(" + DateTime.Now.ToLongTimeString() + ")";
            workingThread.Start();
            WorkingState = WorkingState.Working;
        }

        public void Stop()
        {
            StoppingProcessStarted?.Invoke();

            lock (signal)
            {
                signal.Reset();
            }

            while (workingThread.ThreadState == ThreadState.Running)
                Thread.Sleep(10);

            lock (signal)
            {
                shouldStop = true;
                signal.Set();
            }

            workingThread.Join();

            WorkingState = WorkingState.Stopped;
            StoppingProcessFinished?.Invoke();
        }

        public Thread StopAsync()
        {
            var thread = new Thread(Stop);
            thread.Start();
            return thread;
        }

        public void Pause()
        {
            lock (signal)
            {
                signal.Reset();
            }
            WorkingState = WorkingState.Paused;
        }

        public void Resume()
        {
            lock (signal)
            {
                signal.Set();
            }
            WorkingState = WorkingState.Working;
        }

        public void Restart()
        {
            StopAsync().Join();
            Start();
        }

        public void RestartAsync() => new Thread(Restart).Start();

        #endregion

    }
}
