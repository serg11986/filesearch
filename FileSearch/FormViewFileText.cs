﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace FileSearch
{
    public partial class FormViewFileText : Form
    {
        string fullPath;
        public FormViewFileText(string fullPath)
        {
            InitializeComponent();
            this.fullPath = fullPath;
            Text = fullPath;
        }

        private void FormViewFileText_Load(object sender, EventArgs e)
        {
            try
            {
                tbFileText.Text = File.ReadAllText(fullPath,
                    Encoding.Default);
                tbFileText.Select(tbFileText.TextLength, 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось открыть файл " + fullPath + " :\r\n" + ex.Message);
                Close();
            }
        }
    }
}
