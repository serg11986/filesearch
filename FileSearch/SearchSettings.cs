﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FileSearch
{
    [DataContract]
    public class SearchSettings
    {
        [DataMember]
        public string StartFolder { get; set; } 
            = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        [DataMember]
        public string SearchString { get; set; }

        [DataMember]
        public string FileNameTemplate { get; set; }
    }
}
