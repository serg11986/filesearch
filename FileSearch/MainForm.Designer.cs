﻿namespace FileSearch
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeViewFoundFiles = new System.Windows.Forms.TreeView();
            this.headerFileNameTemplate = new System.Windows.Forms.Label();
            this.tbFileNameTemplate = new System.Windows.Forms.TextBox();
            this.tbStartFolder = new System.Windows.Forms.TextBox();
            this.headerStartFolder = new System.Windows.Forms.Label();
            this.tbSearchString = new System.Windows.Forms.TextBox();
            this.headerSearchString = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.headerCurProcessingFolder = new System.Windows.Forms.Label();
            this.lblCurProcessingFolder = new System.Windows.Forms.Label();
            this.headerCurProcessingFile = new System.Windows.Forms.Label();
            this.lblCurProcessingFile = new System.Windows.Forms.Label();
            this.lblTimeElapsed = new System.Windows.Forms.Label();
            this.headerTimeElapsed = new System.Windows.Forms.Label();
            this.headerFoundFiles = new System.Windows.Forms.Label();
            this.btnChooseStartFolder = new System.Windows.Forms.Button();
            this.lblProcessedFilesCount = new System.Windows.Forms.Label();
            this.headerProcessedFilesCount = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // treeViewFoundFiles
            // 
            this.treeViewFoundFiles.Location = new System.Drawing.Point(9, 31);
            this.treeViewFoundFiles.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.treeViewFoundFiles.Name = "treeViewFoundFiles";
            this.treeViewFoundFiles.Size = new System.Drawing.Size(388, 225);
            this.treeViewFoundFiles.TabIndex = 0;
            this.treeViewFoundFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeViewFoundFiles_MouseDoubleClick);
            // 
            // headerFileNameTemplate
            // 
            this.headerFileNameTemplate.AutoSize = true;
            this.headerFileNameTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerFileNameTemplate.Location = new System.Drawing.Point(418, 31);
            this.headerFileNameTemplate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerFileNameTemplate.Name = "headerFileNameTemplate";
            this.headerFileNameTemplate.Size = new System.Drawing.Size(138, 13);
            this.headerFileNameTemplate.TabIndex = 1;
            this.headerFileNameTemplate.Text = "Шаблон имени файла:";
            // 
            // tbFileNameTemplate
            // 
            this.tbFileNameTemplate.Location = new System.Drawing.Point(420, 47);
            this.tbFileNameTemplate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbFileNameTemplate.Name = "tbFileNameTemplate";
            this.tbFileNameTemplate.Size = new System.Drawing.Size(354, 20);
            this.tbFileNameTemplate.TabIndex = 2;
            // 
            // tbStartFolder
            // 
            this.tbStartFolder.Location = new System.Drawing.Point(420, 94);
            this.tbStartFolder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbStartFolder.Name = "tbStartFolder";
            this.tbStartFolder.Size = new System.Drawing.Size(354, 20);
            this.tbStartFolder.TabIndex = 4;
            // 
            // headerStartFolder
            // 
            this.headerStartFolder.AutoSize = true;
            this.headerStartFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerStartFolder.Location = new System.Drawing.Point(418, 78);
            this.headerStartFolder.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerStartFolder.Name = "headerStartFolder";
            this.headerStartFolder.Size = new System.Drawing.Size(146, 13);
            this.headerStartFolder.TabIndex = 3;
            this.headerStartFolder.Text = "Стартовая директория:";
            // 
            // tbSearchString
            // 
            this.tbSearchString.Location = new System.Drawing.Point(420, 175);
            this.tbSearchString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSearchString.Name = "tbSearchString";
            this.tbSearchString.Size = new System.Drawing.Size(354, 20);
            this.tbSearchString.TabIndex = 6;
            // 
            // headerSearchString
            // 
            this.headerSearchString.AutoSize = true;
            this.headerSearchString.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerSearchString.Location = new System.Drawing.Point(418, 158);
            this.headerSearchString.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerSearchString.Name = "headerSearchString";
            this.headerSearchString.Size = new System.Drawing.Size(117, 13);
            this.headerSearchString.TabIndex = 5;
            this.headerSearchString.Text = "Текст для поиска:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(420, 214);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(82, 41);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "Старт";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(510, 214);
            this.btnPause.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(82, 41);
            this.btnPause.TabIndex = 8;
            this.btnPause.Text = "Пауза";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(600, 214);
            this.btnRestart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(82, 41);
            this.btnRestart.TabIndex = 9;
            this.btnRestart.Text = "Перезапуск";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(690, 214);
            this.btnStop.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(82, 41);
            this.btnStop.TabIndex = 10;
            this.btnStop.Text = "Стоп";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // headerCurProcessingFolder
            // 
            this.headerCurProcessingFolder.AutoSize = true;
            this.headerCurProcessingFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerCurProcessingFolder.Location = new System.Drawing.Point(18, 273);
            this.headerCurProcessingFolder.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerCurProcessingFolder.Name = "headerCurProcessingFolder";
            this.headerCurProcessingFolder.Size = new System.Drawing.Size(310, 13);
            this.headerCurProcessingFolder.TabIndex = 11;
            this.headerCurProcessingFolder.Text = "Текущая/последняя обрабатываемая директория:";
            // 
            // lblCurProcessingFolder
            // 
            this.lblCurProcessingFolder.AutoSize = true;
            this.lblCurProcessingFolder.Location = new System.Drawing.Point(18, 287);
            this.lblCurProcessingFolder.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurProcessingFolder.Name = "lblCurProcessingFolder";
            this.lblCurProcessingFolder.Size = new System.Drawing.Size(95, 13);
            this.lblCurProcessingFolder.TabIndex = 12;
            this.lblCurProcessingFolder.Text = "<поиск не начат>";
            // 
            // headerCurProcessingFile
            // 
            this.headerCurProcessingFile.AutoSize = true;
            this.headerCurProcessingFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerCurProcessingFile.Location = new System.Drawing.Point(18, 310);
            this.headerCurProcessingFile.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerCurProcessingFile.Name = "headerCurProcessingFile";
            this.headerCurProcessingFile.Size = new System.Drawing.Size(273, 13);
            this.headerCurProcessingFile.TabIndex = 13;
            this.headerCurProcessingFile.Text = "Текущий/последний обрабатываемый файл:";
            // 
            // lblCurProcessingFile
            // 
            this.lblCurProcessingFile.AutoSize = true;
            this.lblCurProcessingFile.Location = new System.Drawing.Point(18, 323);
            this.lblCurProcessingFile.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurProcessingFile.Name = "lblCurProcessingFile";
            this.lblCurProcessingFile.Size = new System.Drawing.Size(95, 13);
            this.lblCurProcessingFile.TabIndex = 14;
            this.lblCurProcessingFile.Text = "<поиск не начат>";
            // 
            // lblTimeElapsed
            // 
            this.lblTimeElapsed.AutoSize = true;
            this.lblTimeElapsed.Location = new System.Drawing.Point(18, 396);
            this.lblTimeElapsed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTimeElapsed.Name = "lblTimeElapsed";
            this.lblTimeElapsed.Size = new System.Drawing.Size(95, 13);
            this.lblTimeElapsed.TabIndex = 15;
            this.lblTimeElapsed.Text = "<поиск не начат>";
            // 
            // headerTimeElapsed
            // 
            this.headerTimeElapsed.AutoSize = true;
            this.headerTimeElapsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerTimeElapsed.Location = new System.Drawing.Point(18, 383);
            this.headerTimeElapsed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerTimeElapsed.Name = "headerTimeElapsed";
            this.headerTimeElapsed.Size = new System.Drawing.Size(230, 13);
            this.headerTimeElapsed.TabIndex = 16;
            this.headerTimeElapsed.Text = "Время текущего/последнего поиска:";
            // 
            // headerFoundFiles
            // 
            this.headerFoundFiles.AutoSize = true;
            this.headerFoundFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerFoundFiles.Location = new System.Drawing.Point(9, 15);
            this.headerFoundFiles.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerFoundFiles.Name = "headerFoundFiles";
            this.headerFoundFiles.Size = new System.Drawing.Size(121, 13);
            this.headerFoundFiles.TabIndex = 20;
            this.headerFoundFiles.Text = "Найденные файлы:";
            // 
            // btnChooseStartFolder
            // 
            this.btnChooseStartFolder.Location = new System.Drawing.Point(420, 117);
            this.btnChooseStartFolder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnChooseStartFolder.Name = "btnChooseStartFolder";
            this.btnChooseStartFolder.Size = new System.Drawing.Size(352, 24);
            this.btnChooseStartFolder.TabIndex = 21;
            this.btnChooseStartFolder.Text = "Выбрать/cменить директорию";
            this.btnChooseStartFolder.UseVisualStyleBackColor = true;
            this.btnChooseStartFolder.Click += new System.EventHandler(this.btnChooseStartFolder_Click);
            // 
            // lblProcessedFilesCount
            // 
            this.lblProcessedFilesCount.AutoSize = true;
            this.lblProcessedFilesCount.Location = new System.Drawing.Point(18, 361);
            this.lblProcessedFilesCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProcessedFilesCount.Name = "lblProcessedFilesCount";
            this.lblProcessedFilesCount.Size = new System.Drawing.Size(95, 13);
            this.lblProcessedFilesCount.TabIndex = 23;
            this.lblProcessedFilesCount.Text = "<поиск не начат>";
            // 
            // headerProcessedFilesCount
            // 
            this.headerProcessedFilesCount.AutoSize = true;
            this.headerProcessedFilesCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerProcessedFilesCount.Location = new System.Drawing.Point(18, 347);
            this.headerProcessedFilesCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.headerProcessedFilesCount.Name = "headerProcessedFilesCount";
            this.headerProcessedFilesCount.Size = new System.Drawing.Size(407, 13);
            this.headerProcessedFilesCount.TabIndex = 22;
            this.headerProcessedFilesCount.Text = "Всего обработоно файлов во время текущего/последнего поиска:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 418);
            this.Controls.Add(this.lblProcessedFilesCount);
            this.Controls.Add(this.headerProcessedFilesCount);
            this.Controls.Add(this.btnChooseStartFolder);
            this.Controls.Add(this.headerFoundFiles);
            this.Controls.Add(this.headerTimeElapsed);
            this.Controls.Add(this.lblTimeElapsed);
            this.Controls.Add(this.lblCurProcessingFile);
            this.Controls.Add(this.headerCurProcessingFile);
            this.Controls.Add(this.lblCurProcessingFolder);
            this.Controls.Add(this.headerCurProcessingFolder);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tbSearchString);
            this.Controls.Add(this.headerSearchString);
            this.Controls.Add(this.tbStartFolder);
            this.Controls.Add(this.headerStartFolder);
            this.Controls.Add(this.tbFileNameTemplate);
            this.Controls.Add(this.headerFileNameTemplate);
            this.Controls.Add(this.treeViewFoundFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Поиск файлов";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewFoundFiles;
        private System.Windows.Forms.Label headerFileNameTemplate;
        private System.Windows.Forms.TextBox tbFileNameTemplate;
        private System.Windows.Forms.TextBox tbStartFolder;
        private System.Windows.Forms.Label headerStartFolder;
        private System.Windows.Forms.TextBox tbSearchString;
        private System.Windows.Forms.Label headerSearchString;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label headerCurProcessingFolder;
        private System.Windows.Forms.Label lblCurProcessingFolder;
        private System.Windows.Forms.Label headerCurProcessingFile;
        private System.Windows.Forms.Label lblCurProcessingFile;
        private System.Windows.Forms.Label lblTimeElapsed;
        private System.Windows.Forms.Label headerTimeElapsed;
        private System.Windows.Forms.Label headerFoundFiles;
        private System.Windows.Forms.Button btnChooseStartFolder;
        private System.Windows.Forms.Label lblProcessedFilesCount;
        private System.Windows.Forms.Label headerProcessedFilesCount;
        private System.Windows.Forms.Timer timer;
    }
}

